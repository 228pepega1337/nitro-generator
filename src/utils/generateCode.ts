const characters: string = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789"

export default function generateCode( length ): string {
    let result: string = "";

    for ( let i: number = 0; i < length; i++ ) {
        result += characters.charAt( Math.floor( Math.random() * characters.length ) );
    }

    return result;
}