import generateCode from "./generateCode";

export default function generateLink(): string {
    return "https://discord.gift/" + generateCode( 16 );
}