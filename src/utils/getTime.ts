import moment from "moment";

moment.locale( "ru" );

export default function getTime(): string {
    return moment().format( "llll" );
}
