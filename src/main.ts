import { webhookUrl, amount } from "./config.json";
import { WebhookClient } from "discord.js";
import generateLink from "./utils/generateLink";

const webhookClient = new WebhookClient( { url: webhookUrl } );

webhookClient.send( {
    content: `:green_square: Application launched`,
    username: "LOG"
} );

let i: number = 0;
while ( i < amount ) {
    webhookClient.send( {
        content: generateLink(),
        username: "LINK"
    } );

    i++;
}

webhookClient.send( {
    content: `:red_square: Application stopped`,
    username: "LOG"
} );
